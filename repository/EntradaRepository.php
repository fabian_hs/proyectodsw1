<?php
require_once "database/QueryBuilder.php";
require_once "AutorRepository.php";

class EntradaRepository extends QueryBuilder
{

  public function __construct(string $table="entrada", string $classEntity="Entrada")
  {
    parent::__construct($table, $classEntity);
  }

  public function getAutor(Entrada $entrada): Autor
  {
    $autorRepository = new AutorRepository();

    return $autorRepository->find($entrada->getAutor());
  }

}


 ?>
