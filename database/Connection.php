<?php
/**
 *
 */

class Connection
{

  function __construct()
  {
    // code...
  }

  public static function make()
  {
    $config  = App::get("config")["database"];
    try {
      $opciones = [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_PERSISTENT => true
      ];
      $conection = new PDO($config['connection'].';dbname='.$config['name'],$config['username'],$config['password'],$config['options']);

      return $conection;
    } catch (Exception $e) {
    echo   $e->getMessage(), "\n";
    }
  }
}
?>
