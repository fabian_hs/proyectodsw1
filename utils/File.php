<?php
/**
 *
 */
class File
{

    private $file;
    private $fileName;

    public function __construct(string $fileName, array $arrTypes) {

        $this->file = $_FILES[$fileName];

        $this->fileName = "";

        if (($this->file["name"] =="")) {

            throw new FileException("Debes especificar un fichero", 1);

        }

        if ($this->file["error"] !== UPLOAD_ERR_OK) {

            switch ($this-file["error"]) {

                case UPLOAD_ERR_INI_SIZE:

                case UPLOAD_ERR_FORM_SIZE:


                    throw new FileException("Error de tamaño", 1);

                case UPLOAD_ERR_PARTIAL:

                    throw new FileException("Error de archivo incompleto",1);

                default:

                    throw new FileException("error genérico en la súbida del fichero",1);

                    break;

            }

        }

        if (in_array($this->file["type"], $arrTypes)===false) {

            throw new FileException("error de tipo",1);

        }

    }

    /**
     * Get the value of File Name
     *
     * @return mixed
     */
    public function getFileName()
    {
        return $this->file['name'];
    }

    public function saveUploadFile($ruta)
    {
      if (is_uploaded_file($this->file['tmp_name'])){

        $rutaArchivo = $ruta.$this->file['name'];

        if(is_file($rutaArchivo)){
          $idUnico = time();
          $this->file['name'] = $idUnico."_".$this->file["name"];
          $rutaArchivo = $ruta.$this->file['name'];
        }

        if(move_uploaded_file($this->file['tmp_name'], $rutaArchivo) === false){
          throw new FileException("No se ha podido mover el fichero al destino especificado.",1);
        }
      }else{
        throw new FileException("El archivo no se ha subido mediante un formulario",1);
      }
    }

    public function copyFile($origen,$destino)
    {
      $origen = $origen.$this->file['name'];
      $destino = $destino.$this->file['name'];
      if (is_file($origen)){
        if (!is_file($destino)){
          if (!copy($origen,$destino)){
            throw new FileException("No se ha podido copiar el fichero.",1);
          }
        }else{
          throw new FileException("El fichero $destino ya existe.",1);
        }

      }else{
        throw new FileException("No existe el fichero $origen .",1);
      }
    }

}


 ?>
