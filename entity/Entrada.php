<?php

/**
 *
 */
class Entrada implements IEntity
{
  private $id;
  private $titulo;
  private $contenido;
  private $categoria;
  private $autor;

  function __construct($titulo='', $contenido='', $categoria='', $autor='')
  {
    $this->id = null;
    $this->titulo = $titulo;
    $this->contenido = $contenido;

    $this->categoria = $categoria;
    $this->autor = $autor;
  }

    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Titulo
     *
     * @return mixed
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set the value of Titulo
     *
     * @param mixed $titulo
     *
     * @return self
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get the value of Contenido
     *
     * @return mixed
     */
    public function getContenido()
    {
        return $this->contenido;
    }

    /**
     * Set the value of Contenido
     *
     * @param mixed $contenido
     *
     * @return self
     */
    public function setContenido($contenido)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get the value of Categoria
     *
     * @return mixed
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set the value of Categoria
     *
     * @param mixed $categoria
     *
     * @return self
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get the value of Autor
     *
     * @return mixed
     */
    public function getAutor()
    {
        return $this->autor;
    }

    /**
     * Set the value of Autor
     *
     * @param mixed $autor
     *
     * @return self
     */
    public function setAutor($autor)
    {
        $this->autor = $autor;

        return $this;
    }

    public function toArray(): array{
      return[
        "id"=>$this->getId(),
        "titulo"=>$this->getTitulo(),
        "contenido"=>$this->getContenido(),
        "categoria"=>$this->getCategoria(),
        "autor"=>$this->getAutor()
      ];
    }

}

 ?>
