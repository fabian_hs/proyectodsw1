<?php
/**
 *
 */

class Categoria implements IEntity
{
  private $id;
  private $nombre;
  private $descripcion;


  function __construct($nombre = '',$descripcion = '')
  {
    $this->nombre = $nombre;
    $this->descripcion = $descripcion;
    $this->id= null;

  }

    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Nombre
     *
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of Nombre
     *
     * @param mixed $nombre
     *
     * @return self
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of Num Imagenes
     *
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set the value of Num Imagenes
     *
     * @param mixed $descripcion
     *
     * @return self
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function toArray(): array{
      return[
        "id"=>$this-> getId(),
        "nombre"=>$this->getNombre(),
        "descripcion"=>$this->getDescripcion()
      ];
    }

}

 ?>
