<?php
/**
 *
 */

require_once "database/IEntity.php";

class Autor implements IEntity
{
  private $id;
  private $nombre;
  private $fecha;
  private $imagen;
  private $author;
  private $descripcion;
  private $email;


  function __construct($nombre = '', $fecha = '', $imagen = '', $author ='', $descripcion = '',$email = '')
  {
    $this->nombre = $nombre;
    $this->descripcion = $descripcion;
    $this->fecha = $fecha;
    $this->author = $author;
    $this->imagen = $imagen;
    $this->email = $email;
    $this->id= null;

  }
    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Nombre
     *
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of Nombre
     *
     * @param mixed $nombre
     *
     * @return self
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of Fecha
     *
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set the value of Fecha
     *
     * @param mixed $fecha
     *
     * @return self
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get the value of Imagen
     *
     * @return mixed
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set the value of Imagen
     *
     * @param mixed $imagen
     *
     * @return self
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get the value of Author
     *
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set the value of Author
     *
     * @param mixed $author
     *
     * @return self
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get the value of Descripcion
     *
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set the value of Descripcion
     *
     * @param mixed $descripcion
     *
     * @return self
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    const RUTA_IMAGENES="img/user/";

    public function getURLPortGallery() : string{

      return self::RUTA_IMAGENES. $this->getNombre();

    }

    public function toArray(): array{
      return[
        "id"=>$this-> getId(),
        "nombre"=>$this->getNombre(),
        "fecha"=> $this->getFecha(),
        "descripcion"=>$this->getDescripcion(),
        "imagen"=>$this->getImagen(),
        "author"=>$this->getAuthor(),
        "email"=>$this->getEmail()
      ];
    }


    /**
     * Get the value of Email
     *
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of Email
     *
     * @param mixed $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

}
