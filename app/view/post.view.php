<?php
include 'partials/inicio_doc.part.php';
include 'partials/nav.part.php';
?>
  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/post-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="post-heading">
            <h1>Esta es la primera publicacion </h1>
            <h2 class="subheading">En este blog vamos a publicar todo tipo de noticias, para el gusto de tod@s.
            <span class="meta">Publicado por
              <a href="#">Fabian Hernandez Stork</a>
              en Diciembre 13, 2019</span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Post Content -->
  <article>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">

          <?php foreach ($arrayEntrada as $key):?>
            <div class="post-preview">
              <p>
                <h2 class="post-title">
                  <?php echo $key->getTitulo();?>
                </h2>
                <p class="h4"><?php echo $key->getContenido();?></p>
              </p>
              <pclass="h5"><em>Publicado por
                <?php echo $entradaRepository->getAutor($key)->getNombre();?></em></p>
            </div>
            <hr>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </article>

  <hr>

  <?php
  include 'partials/fin_doc.part.php';
  ?>
