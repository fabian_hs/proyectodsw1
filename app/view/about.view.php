<?php
include 'partials/inicio_doc.part.php';
include 'partials/nav.part.php';
?>
  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/about-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="page-heading">
            <h1>Miembros</h1>
            <span class="subheading">Grupo de personas</span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <?php foreach ($arrayAutor as $key):?>

          <div class="card flex-md-row mb-4 box-shadow h-md-250">
                      <div class="card-body d-flex flex-column align-items-start">
                        <strong class="d-inline-block mb-2 text-primary"><?php echo $key->getAuthor(); ?></strong>
                        <h3 class="mb-0">
                          <a class="text-dark" href="#" name=""><?php echo $key->getNombre(); ?></a>
                        </h3>
                        <div class="mb-1 text-muted"><?php echo $key->getFecha(); ?></div>
                        <p class="card-text mb-auto"><?php echo $key->getDescripcion(); ?></p>
                        <div class="mb-1 text-muted" name="email"><?php echo $key->getEmail(); ?></div>
                      </div>
                      <img class="card-img-right flex-auto d-none d-md-block" data-src="holder.js/200x250?theme=thumb" alt="Thumbnail [200x250]" style="width: 200px; height: 250px;" src="./img/user/<?php echo $key->getImagen(); ?>" data-holder-rendered="true">
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
  <hr>
  <?php
  include 'partials/fin_doc.part.php';
  ?>
