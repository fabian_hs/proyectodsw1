<?php
include 'partials/inicio_doc.part.php';
include 'partials/nav.part.php';
?>
  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/contact-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="page-heading">
            <h1>Crear, Modificar o Eliminar Autores</h1>
            <span class="subheading">¿Que quieres hacer?</span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <p>Aqui tienes la posibilidad de crear o modificar como tambien borrar</p>
        <?php
        $numero = count($er);

        if($numero > 0){
          echo "<br>";
          for ($i= 0; $i <$numero;$i++){
            echo "<div class=\"alert alert-danger\">
            <button type='button' class='clase' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>x</span>
            </button>
             <strong>Error </strong>$er[$i]</div>";
          }
        }
        ?>
        <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') : ?>
        <div class="alert alert-<?= empty($errores) ? 'info' : 'danger'; ?> alert-dismissible" role="alert">
            <button type="button" class="clase" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">x</span>
            </button>
            <?php if(empty($errores)) : ?>
            <?= $mensaje ?>
            <?php else : ?>

                <?php foreach($errores as $error) : ?>
                <?= $error ?>
                <?php endforeach; ?>

            <?php endif; ?>
        </div>
        <?php endif; ?>

        <p class="help-block text-danger"></p>
        <form class="form-horizontal"  method="POST"
        enctype="multipart/form-data" action="<?=$_SERVER["REQUEST_URI"] ?>">

            <div class="form-group controls">
              <label class="text-secondary">Nombre del Autor</label>
              <input type="text" class="form-control" placeholder="Nombre del Autor" name="nombre">
              <p class="help-block text-danger"></p>
            </div>
            <div class="form-group">
              <label class="text-secondary">Fecha de nacimiento</label>
              <input type="date" class="form-control" placeholder="" name="fecha">
              <p class="help-block text-danger"></p>
            </div>
          <div class="control-group">
              <label class="text-secondary">Email</label>
              <input type="email" class="form-control" placeholder="Email" name="email">
              <p class="help-block text-danger"></p>
          </div>
          <div class="control-group">
              <label class="text-secondary">Descripcion</label>
              <textarea class="form-control" name="descripcion" rows="3" placeholder="Descripcion"></textarea>
              <p class="help-block text-danger"></p>
          </div>
          <div class="form-row">
            <div class="form-group col-md-4">
              <div class="form-group">
              <label class="text-secondary">Puesto</label>
              <select class="form-control" name="autor" >
                <option>Elige un puesto</option>
                <option>Jefe</option>
                <option>Editor</option>
              </select>
              </div>
            </div>
            <div class="form-group col-md-8">
              <div class="form-group">
                <label class="text-secondary">Imagen</label>
                <input class="form-control-file" name="imagen" type="file"><p class="help-block text-danger"></p>
              </div>
            </div>
          <br>
          <div class="form-group">
            <button type="submit" class="btn btn-primary" name="enviar" value="true">Enviar y publicar</button>
            </div>
          </div>
        </form>

      </div>
    </div>
    <div class="row">

    <div class="col-lg-8 col-md-10 mx-auto">
      <form class="form-horizontal"  method="POST"
      enctype="multipart/form-data" action="<?=$_SERVER["REQUEST_URI"] ?>">
        <table class="table">
          <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Nombre</th>
              <th scope="col">Fecha</th>
              <th scope="col">Email</th>
              <th scope="col">Descripcion</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($arrayAutor as $key):?>
              <tr>
                <th scope="row"><?php echo $key->getId(); ?></th>
                <td><?php echo $key->getNombre();  ?></td>
                <td><?php echo $key->getFecha(); ?></td>
                <td><?php echo $key->getEmail(); ?></td>
                <td><?php echo $key->getDescripcion();?></td>

              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </form>
      </div>
    </div>
  </div>
  <hr>
  <?php
  include 'partials/fin_doc.part.php';
  ?>
