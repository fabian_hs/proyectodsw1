<?php
include 'partials/inicio_doc.part.php';
include 'partials/nav.part.php';
?>
  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/contact-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="page-heading">
            <h1>Crear, Modificar o Eliminar Categorias</h1>
            <span class="subheading">¿Que quieres hacer?</span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <p>Aqui tienes la pocibilidad de crear o modificar como tambien borrar</p>
        <?php
        $numero = count($error);

        if($numero > 0){
          echo "<p>";
          for ($i= 0; $i <$numero;$i++){
            echo "<div class=\"alert alert-danger\">
             <strong>Error </strong>$error[$i]</div>";
          }
        }

        ?>

        <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') : ?>
        <div class="alert alert-<?= empty($errores) ? 'info' : 'danger'; ?> alert-dismissible" role="alert">
            <button type="button" class="clase" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">x</span>
            </button>
            <?php if(empty($errores)) : ?>
            <p><?= $mensaje ?></p>
            <?php else : ?>
            <ul>
                <?php foreach($errores as $error) : ?>
                <li><?= $error ?></li>
                <?php endforeach; ?>
            </ul>
            <?php endif; ?>
        </div>
        <?php endif; ?>
        <p class="help-block text-danger"></p>
        <form class="form-horizontal"  method="POST" enctype="multipart/form-data" action="<?=$_SERVER["REQUEST_URI"] ?>">
            <div class="form-group controls">
              <label class="text-secondary">Nombre de la Categoria</label>
              <input type="text" class="form-control" placeholder="Nombre del Autor" name="categoria">
              <p class="help-block text-danger"></p>
            </div>
            <div class="form-group">
              <label class="text-secondary">Descripcion</label>
              <textarea class="form-control" name="descripcion" rows="3" placeholder="Descripcion"></textarea>
              <p class="help-block text-danger"></p>
            </div>
            <div class="form-group col-md-1"></div>
            <div class="form-group col-md-3">
            <button type="submit" class="btn btn-primary" name="enviar" value="true">Enviar y publicar</button>
          </div>
          </div>
          <br>

          <div class="form-group">

          </div>
        </form>
      </div>
    </div>
  </div>

  <hr>

  <?php
  include 'partials/fin_doc.part.php';
  ?>
