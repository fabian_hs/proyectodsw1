<body>
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand" href="./">Blog de Fabian</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="./">Inicio</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="post2">Publicaciones</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="about">Miembros</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="formulario">Redactar Noticias</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="categoria">Categoria</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="autor">Autor</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
