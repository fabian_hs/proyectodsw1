<?php
include 'partials/inicio_doc.part.php';
include 'partials/nav.part.php';
?>
  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/contact-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="page-heading">
            <h1>Redactar Nueva Entrada</h1>
            <span class="subheading">¿Quieres redactar una nueva entrada?</span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <p>¿Quieres redactar una nueva entrada? Solo tienes que rellenar el formulario que tienes a continuacion para publicarlo.</p>
        <?php
        $numero = count($er);

        if($numero > 0){
          echo "<p>";
          for ($i= 0; $i <$numero;$i++){
            echo "<div class=\"alert alert-danger\">
             <strong>Error </strong>$er[$i]</div>";
          }
        }

        ?>
        <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') : ?>
        <div class="alert alert-<?= empty($errores) ? 'info' : 'danger'; ?> alert-dismissible" role="alert">
            <button type="button" class="clase" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">x</span>
            </button>
            <?php if(empty($errores)) : ?>
            <p><?= $mensaje ?></p>
            <?php else : ?>
            <p>
                <?php foreach($errores as $error) : ?>
                <?= $error ?>
                <?php endforeach; ?>
            </p>
            <?php endif; ?>
        </div>
        <?php endif; ?>
        <p class="help-block text-danger"></p>
        <form class="form-horizontal"  method="post">
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Titulo de la Entrada</label>
              <input type="text" class="form-control" placeholder="Titulo de la Entrada" name="titulo">
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Contenido de la Entrada</label>
              <textarea class="form-control" name="contenido" rows="3" placeholder="Contenido de la Entrada"></textarea>
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-4">
              <label class="text-secondary">Autor</label>
              <select class="form-control" name="autor" >
                <option>Elige uno</option>

                <?php foreach ($arrayAutor as $key):

                 ?>
                 <option value="<?php echo $key->getId() ?>" ><?php echo $key->getNombre(); ?></option>

                <?php endforeach; ?>
              </select>
              <p class="help-block text-danger"></p>
            </div>
            <div class="form-group col-md-4">
              <label class="text-secondary">Categoria</label>
              <select class="form-control" name="categoria">
                <option>Elige uno</option>

                <?php foreach ($arrayCategoria as $key):

                 ?>
                 <option value="<?php echo $key->getId() ?>"><?php echo $key->getNombre(); ?></option>

                <?php endforeach; ?>
              </select>
            </div>
            <div class="form-group col-md-1"></div>
            <div class="form-group col-md-3">

            <button type="submit" class="btn btn-primary" name="enviar" value="true">Enviar y publicar</button>
          </div>
          </div>
          <br>

          <div class="form-group">

          </div>
        </form>
      </div>
    </div>
  </div>

  <hr>

  <?php
  include 'partials/fin_doc.part.php';
  ?>
