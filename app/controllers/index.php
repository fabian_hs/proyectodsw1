<?php
require_once 'utils/File.php';

require_once 'exceptions/FileException.php';
require_once 'exceptions/QueryException.php';
require_once 'exceptions/AppException.php';

require_once 'core/App.php';

require_once 'database/Connection.php';
require_once 'database/QueryBuilder.php';

try{

  $connection = App::getConnection();

}catch (QueryException $queryException) {

    $errores [] = $queryException->getMessage();

}

require __DIR__ . "/../view/index.view.php";
 ?>
