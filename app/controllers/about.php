<?php
require_once 'database/Connection.php';
require_once 'database/QueryBuilder.php';
require_once 'database/IEntity.php';

require_once 'entity/Autor.php';

require_once 'exceptions/AppException.php';
require_once 'exceptions/QueryException.php';

require_once 'core/App.php';

require_once 'repository/AutorRepository.php';
require_once 'utils/File.php';
try{
  $config = App::getConnection();

  $autorRepository = new AutorRepository();

  //Select
  $arrayAutor = $autorRepository->findAll();

}catch (QueryException $queryException) {

      $errores [] = $queryException->getMessage();

  }
  catch (AppException $appException) {

    throw new AppException("No se ha podido conectar con la BBDD");

  }
require __DIR__ . "/../view/about.view.php";
 ?>
