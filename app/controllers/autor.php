<?php
require_once 'database/Connection.php';
require_once 'database/QueryBuilder.php';
require_once 'database/IEntity.php';

require_once 'entity/Autor.php';

require_once 'exceptions/AppException.php';
require_once 'exceptions/QueryException.php';
require_once 'exceptions/FileException.php';


require_once 'core/App.php';

require_once 'repository/AutorRepository.php';
require_once 'utils/File.php';

$er = array(); // Array de errores

try{
  $config = App::getConnection();

  $autorRepository = new AutorRepository();

  $arrayAutor = $autorRepository->findAll();

  $enviar= htmlspecialchars($_POST['enviar'] ?? false);
  $nombre= htmlspecialchars($_POST['nombre'] ?? null);
  $fecha= htmlspecialchars($_POST['fecha'] ?? null);
  $email= htmlspecialchars($_POST['email'] ?? null);
  $descripcion= htmlspecialchars($_POST['descripcion'] ?? null);
  $ID = htmlspecialchars($_POST['id'] ?? null);
  $autor = htmlspecialchars($_POST['autor'] ?? "Elige un puesto");


  if ($_SERVER["REQUEST_METHOD"]==="POST") {
  /**/

  $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];
  $imagen = new File("imagen", $tiposAceptados);

  $imagen->saveUploadFile(Autor::RUTA_IMAGENES);

  if($enviar){
    if ($nombre == null){
      array_push($er, "Tiene que a ver  un nombre.");
    }
    if ($fecha == null){
      array_push($er, "Tiene que a ver  una fecha.");
    }
    if ($email == null){
      array_push($er, "Tiene que a ver  un email.");
    }
    if ($descripcion == null){
      array_push($er, "Tiene que a ver  una descripcion.");
    }

    if ($autor == "Elige un puesto"){
      array_push($er, "Tiene que elegir un puesto.");
    }

    if(count($er) == 0){
      $autor = new Autor($nombre, $fecha, $imagen->getFileName(), $autor, $descripcion,$email);
      $autorRepository ->save($autor);
      $mensaje = "<strong>Felicidades</strong> Se ha guardado en la BBDD.";
    }else{
      $errores [] = "Error";
    }
}
  }
} catch (FileException $fileException) {

    $errores [] = $fileException->getMessage();

}
catch (QueryException $queryException) {

    $errores [] = $queryException->getMessage();

}
catch (AppException $appException) {

  throw new AppException("No se ha podido conectar con la BBDD");

}


require __DIR__ . "/../view/autor.view.php";
 ?>
