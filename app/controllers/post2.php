<?php

require_once 'database/Connection.php';
require_once 'database/QueryBuilder.php';
require_once 'database/IEntity.php';

require_once 'entity/Autor.php';
require_once 'entity/Categoria.php';
require_once 'entity/Entrada.php';

require_once 'exceptions/AppException.php';
require_once 'exceptions/QueryException.php';

require_once 'core/App.php';

require_once 'repository/AutorRepository.php';
require_once 'repository/CategoriaRepository.php';
require_once 'repository/EntradaRepository.php';
require_once 'utils/File.php';

try{
  $config = App::getConnection();

  $entradaRepository = new EntradaRepository();
  $autorRepository = new AutorRepository();
  $categoriaRepository = new CategoriaRepository();

  //Select
  $arrayEntrada = $entradaRepository->findAll();
  $arrayAutor = $autorRepository->findAll();
  $arrayCategoria = $categoriaRepository->findAll();

}catch (QueryException $queryException) {

      $errores [] = $queryException->getMessage();

  }
  catch (AppException $appException) {

    throw new AppException("No se ha podido conectar con la BBDD");

  }



require __DIR__ . "/../view/post.view.php";
 ?>
