<?php
require_once 'database/Connection.php';
require_once 'database/QueryBuilder.php';
require_once 'database/IEntity.php';

require_once 'entity/Categoria.php';

require_once 'exceptions/AppException.php';
require_once 'exceptions/QueryException.php';

require_once 'core/App.php';

require_once 'repository/CategoriaRepository.php';

$error = Array();

try{
  $config = App::getConnection();

  $categoriaRepository = new CategoriaRepository();

  if ($_SERVER["REQUEST_METHOD"]==="POST") {

    $categoria= htmlspecialchars($_POST['categoria'] ?? null);
    $descripcion = htmlspecialchars($_POST['descripcion'] ?? null);
    $enviar = htmlspecialchars($_POST['enviar'] ?? false);

    if($enviar){
      if ($categoria == null){
        array_push($error, "Tiene que a ver  un nombre en la categoria.");
      }
      if ($descripcion == null){
        array_push($error, "Tiene que a ver  una descripcion para la categoria.");
      }
    }
    if(count($error) == 0){
      $categoria = new Categoria($categoria, $descripcion);
      $categoriaRepository->save($categoria);
      $mensaje = "se ha guardado en la BBDD.";
    }else{
      $mensaje = "Error";
    }
  }


} catch (FileException $fileException) {

    $errores [] = $fileException->getMessage();

}
catch (QueryException $queryException) {

    $errores [] = $queryException->getMessage();

}
catch (AppException $appException) {

  throw new AppException("No se ha podido conectar con la BBDD");

}

require __DIR__ . "/../view/categoria.view.php";
 ?>
