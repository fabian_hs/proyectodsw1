<?php

require_once 'database/Connection.php';
require_once 'database/QueryBuilder.php';
require_once 'database/IEntity.php';

require_once 'entity/Autor.php';
require_once 'entity/Categoria.php';
require_once 'entity/Entrada.php';

require_once 'exceptions/AppException.php';
require_once 'exceptions/QueryException.php';

require_once 'core/App.php';

require_once 'repository/AutorRepository.php';
require_once 'repository/CategoriaRepository.php';
require_once 'repository/EntradaRepository.php';


$er = array(); // Array de errores

try{
  $config = App::getConnection();

  $autorRepository = new AutorRepository();
  $categoriaRepository = new CategoriaRepository();
  $entradaRepository = new EntradaRepository();
  //Select
  $arrayAutor = $autorRepository->findAll();
  $arrayCategoria = $categoriaRepository->findAll();

  $titulo = htmlspecialchars($_POST['titulo'] ?? null);
  $contenido= htmlspecialchars($_POST['contenido'] ?? null);
  $autor = htmlspecialchars($_POST['autor'] ?? null);

  $categoria = htmlspecialchars($_POST['categoria'] ?? null);

  $enviar = htmlspecialchars($_POST['enviar'] ?? false);

  if ($enviar){
    if ($titulo == null){
      array_push($er, "Tiene que a ver  un titulo asignado a esta entrada.");
    }
    if ($contenido == null){
      array_push($er, "Tiene que a ver un contenido asignado a esta entrada.");
    }

    if ($autor == null){
      array_push($er, "Tiene que a ver  un autor asignada a esta entrada.");
    }
    if ($categoria == null){
      array_push($er, "Tiene que a ver  una categoria asignada a esta entrada.");
    }



  }
  if(count($er) == 0){
    $entrada = new Entrada($titulo, $contenido, $categoria, $autor);
    $entradaRepository ->save($entrada);
    $mensaje = "<strong>Felicidades</strong> Se ha guardado en la BBDD.";
  }else{
    $errores [] = "Error";
  }



} catch (FileException $fileException) {

    $errores [] = $fileException->getMessage();

}
catch (QueryException $queryException) {

    $errores [] = $queryException->getMessage();

}
catch (AppException $appException) {

  throw new AppException("No se ha podido conectar con la BBDD");

}


require __DIR__ . "/../view/formulario.view.php";
 ?>
